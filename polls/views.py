from django.shortcuts import render
from django.http import HttpResponse
from mama_cas.views import LoginView,LogoutView
from django.contrib.auth import login
from mama_cas.models import ServiceTicket
from mama_cas.utils import redirect




from inspect import isfunction

from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

class _cbv_decorate(object):
    def __init__(self, dec):
        self.dec = method_decorator(dec)

    def __call__(self, obj):
        obj.dispatch = self.dec(obj.dispatch)
        return obj

def patch_view_decorator(dec):
    def _conditional(view):
        if isfunction(view):
            return dec(view)
        return _cbv_decorate(dec)(view)
    return _conditional



def index(request):
    return HttpResponse("CAS Server...")

# @patch_view_decorator(csrf_exempt)
class logoutView(LogoutView):

    def get(self, request, *args, **kwargs):
        # request.session.delete()
        return super(logoutView, self).get(request, *args, **kwargs)

@patch_view_decorator(csrf_exempt)
class loginView(LoginView):
    """Allow a person to login."""

    def form_valid(self, form):
        login(self.request, form.user)
        if form.cleaned_data.get('warn'):
            self.request.session['warn'] = True

        service = self.request.GET.get('service')
        if service:
            st = ServiceTicket.objects.create_ticket(service=service,
                                                     user=self.request.user,
                                                     primary=True)
            return redirect(service, params={'ticket': st.ticket})
        return redirect('cas_login')
