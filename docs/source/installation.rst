************
Installation
************

Please follow the Below steps
=============================

1. git remote add origin https://rishabh_systango@bitbucket.org/rishabh_systango/demo-cas-server.git
2. cd demo-cas-server
3. virtualenv env_cas
4. source env_cas/bin/activate
5. pip install -r requirements.txt
6. python manage.py migrate
7. python manage.py runserver




Settings
========

settings.py::

MAMA_CAS_SERVICES :- This is a list of the services that would be hitting our CAS server.

MAMA_CAS_SERVICES = [
    {
        'SERVICE': '^http:\/\/.*.ngrok.io/', # the regex for the services that we want to validate against CAS
        'CALLBACKS': [
            'mama_cas.callbacks.user_name_attributes',
        ],
        'PROXY_ALLOW': True,
        'PROXY_PATTERN': '^http:\/\/.*.ngrok.io/',
        'LOGOUT_ALLOW': True,
        'LOGOUT_URL' : '/logout/',
    }
]

