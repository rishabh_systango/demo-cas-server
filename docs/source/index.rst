.. CAS_SERVER documentation master file, created by
   sphinx-quickstart on Mon Oct 24 12:51:06 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CAS_SERVER's documentation!
======================================

Contents:

.. toctree::
   :maxdepth: 1

    Installation <installation>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

