"""
Django settings for cas_demo project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '4w#w+yz)^4)plw#d#a+$0@rgl8567d@df9j6spk!ycilhs*%(w'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.sites',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'mama_cas',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
)

SITE_ID = 1

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'cas_demo.urls'

WSGI_APPLICATION = 'cas_demo.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]


MAMA_CAS_SERVICES = [
    {
        'SERVICE': '^http:\/\/.*.ngrok.io/',
        'CALLBACKS': [
            'mama_cas.callbacks.user_name_attributes',
        ],
        'PROXY_ALLOW': True,
        'PROXY_PATTERN': '^http:\/\/.*.ngrok.io/',
        'LOGOUT_ALLOW': True,
        'LOGOUT_URL' : '/logout/',
    }
]


# MAMA_CAS_SERVICES = [
#     {
#         'SERVICE': '^http://b7ed0670.ngrok.io/',
#         'CALLBACKS': [
#             'mama_cas.callbacks.user_name_attributes',
#         ],
#         'PROXY_ALLOW': True,
#         'PROXY_PATTERN': '^http://b7ed0670.ngrok.io/',
#         'LOGOUT_ALLOW':True,
#         'LOGOUT_URL' : 'http://b7ed0670.ngrok.io/logout/'
#     },
#     {
#         'SERVICE': '^http://4841b173.ngrok.io/',
#         'CALLBACKS': [
#             'mama_cas.callbacks.user_name_attributes',
#         ],
#         'PROXY_ALLOW': True,
#         'PROXY_PATTERN': '^http://4841b173.ngrok.io/',
#         'LOGOUT_ALLOW':True,
#         'LOGOUT_URL' : 'http://4841b173.ngrok.io/logout/'
#     }
# ]



MAMA_CAS_ENABLE_SINGLE_SIGN_OUT = True

AUTHENTICATION_BACKENDS = (
    # Needed to login by username in Django admin, regardless of `allauth`
    'django.contrib.auth.backends.ModelBackend',

    # `allauth` specific authentication methods, such as login by e-mail
    'allauth.account.auth_backends.AuthenticationBackend',
)




# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
