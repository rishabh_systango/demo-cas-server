from django.conf.urls import patterns, include, url
from django.views.decorators.csrf import csrf_exempt
from django.contrib import admin
admin.autodiscover()
from polls import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'cas_demo.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    #url(r'^admin\/?$',include('django.contrib.admin.site.urls')),
    url(r'^accounts/', include('allauth.urls')),
    # url(r'^logout/?$', views.logoutView.as_view(), name='logout'),
    url(r'^login1/?$', csrf_exempt(views.loginView.as_view()), name='login'),
    url(r'', include('mama_cas.urls'))
)
